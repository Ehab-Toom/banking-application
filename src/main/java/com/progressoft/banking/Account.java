package com.progressoft.banking;


public interface Account {


    void deposit(int amount);

    void withdraw(int amount);

    void printPreviousTransaction();

    int getBalance();

    String getCustomerName();

    int getCustomerAccountNumber();

    AccountTransaction getAccountTransaction();

    void setAccountTransaction(AccountTransaction transaction);
}