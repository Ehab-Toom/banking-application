package com.progressoft.banking;


import java.util.Objects;


public class AccountImpl implements Account {
    private final String customerName;         //your name
    private final int customerAccountNumber;   //ATM number
    private int balance;
    private int lastOP;
    private AccountTransaction accountTransaction;
    private Boolean isLastOpDeposit;

    public AccountImpl(String customerName, int customerAccountNumber) {
        this.customerName = customerName;
        this.customerAccountNumber = customerAccountNumber;
        accountTransaction=new AccountTransaction();
    }
    @Override
    public void setAccountTransaction(AccountTransaction accountTransaction) {
        this.accountTransaction = accountTransaction;
    }

    @Override
    public void deposit(int amount) {

        if (amount > 0) {
            balance = balance + amount;
            System.out.println("Your current balance is now : " + balance);
            isLastOpDeposit = true;
            lastOP = amount;
        } else {
            System.out.println("You don't have money to deposit");
        }
    }

    @Override
    public void withdraw(int amount) {

        if (amount < balance) {
            balance -= amount;
            System.out.println("Your current balance is now : " + balance);

        } else {
            System.out.println("Insufficient Funds!");

            isLastOpDeposit = false;
            lastOP = amount;
        }
    }

    public void printPreviousTransaction() {

        String message = Objects.isNull(isLastOpDeposit) ? "no operations yet!" :
                (isLastOpDeposit ? "Deposit (+)" : "Withdraw (-) ") + lastOP;
        System.out.println(message);
    }

    @Override
    public int getBalance() {

        return balance;
    }

    @Override
    public String getCustomerName() {

        return customerName;
    }

    @Override
    public int getCustomerAccountNumber() {

        return customerAccountNumber;
    }

    @Override
    public AccountTransaction getAccountTransaction() {

        return accountTransaction;
    }
}