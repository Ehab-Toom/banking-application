package com.progressoft.banking;


public class AccountTransaction {


    private String transName;
    private String amount;

    public AccountTransaction() {
    }

    public AccountTransaction(String transName, String amount) {

        this.transName = transName;
        this.amount = amount;
    }

    public String getTransName() {

        return transName;
    }

    public String getAmount() {

        return amount;
    }
}