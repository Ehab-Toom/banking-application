package com.progressoft.banking;

import java.io.*;
import java.util.*;


public class BankSystem {


    private final ArrayList<Account> accountList;
    public List<String> records = new ArrayList<>();

    public BankSystem() {

        this.accountList = new ArrayList<>();
    }

    static class MyListToCsvString {

        public String printAccounts(ArrayList<Account> accounts) {

            StringBuilder sb = new StringBuilder();
            String line;
            for (Account account : accounts) {
                line = account.getCustomerName() + ","
                        + account.getCustomerAccountNumber()
                        + "," + account.getBalance() + ","
                        + account.getAccountTransaction().getTransName()
                        + "," + account.getAccountTransaction().getAmount();

                sb.append(line).append("\n");
            }
            return sb.toString();
        }
    }

    public void showMenu() {       //Start

        Scanner input = new Scanner(System.in);
        System.out.println("welcome to our banking system, please insert your account number: ");
        int accountNum = 0;
        try {
            accountNum = input.nextInt();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        Account account = findAccount(accountNum, accountList);    //Log in
        System.out.println("Welcome MR/MISS : " + account.getCustomerName()
                 + " your account number is : " + account.getCustomerAccountNumber());
        if (account == null) {
            System.out.println("account does not exist, please try again");
            return;
        }
            System.out.println("A) Check Balance");
            System.out.println("B) Deposit to a bank account");
            System.out.println("C) Withdraw to bank account");
            System.out.println("D) getPreviousTransaction");
            System.out.println("E) LogOut");
            System.out.println("F) Quit");
            do {
                System.out.println();
                System.out.print("Please enter the transaction code : ");
                char userChoice = input.next().charAt(0);
                switch (userChoice) {
                    case 'A':
                        System.out.println("Check Balance");
                        System.out.println("Your balance is : " + account.getBalance());
                        break;
                    case 'B':
                        System.out.println("deposit you money");
                        System.out.println("Enter a deposit amount");
                        int num = input.nextInt();
                        System.out.println();
                        account.deposit(num);
                        account.setAccountTransaction(new AccountTransaction(
                                "DEPOSITE", String.valueOf(num)));
                        break;
                    case 'C':
                        System.out.println("How much withdraw?");
                        int num2 = input.nextInt();
                        account.withdraw(num2);
                        account.setAccountTransaction(new AccountTransaction(
                                "WITHDRAW", String.valueOf(num2)));
                        break;
                    case 'D':
                        System.out.println("Your last operation ...");
                        account.printPreviousTransaction();
                        break;
                    case 'E':
                        System.out.println("Thank you for using my BankSystem");   //Log out
                        showMenu();
                        break;
                    case 'F':
                        return;
                    default:
                        System.out.println("Wrong entry");     //Finish
                }
            }
            while (true);    //End
        }

    public Account findAccount(int accountNum, List<Account> accounts) {   //Search account

        for (Account account : accounts) {
            if (account.getCustomerAccountNumber() == accountNum) {
                return account;
            }
        }
        return null;
    }

    protected void register(Account account) {

        this.accountList.add(account);
    }

    public void printAccounts() {

        MyListToCsvString mtc = new MyListToCsvString();
        System.out.println(mtc.printAccounts(accountList));
    }

    public void csvFile() throws IOException {     //Print info in CSV

        String csvFile = "./file-information.csv";
        String line;
        for (Account account : accountList) {
            line = account.getCustomerName() + ","
                    + account.getCustomerAccountNumber() + "," + account.getBalance()
                    + "," + account.getAccountTransaction().getTransName()
                    + "," + account.getAccountTransaction().getAmount()+"\n";
            records.add(line);
        }
        FileWriter writer = new FileWriter(csvFile);
        CsvFile.writeLine(writer, records);
        writer.flush();
        writer.close();
    }
}