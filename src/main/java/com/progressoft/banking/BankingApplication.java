package com.progressoft.banking;


import java.io.IOException;


public class BankingApplication {


    public static void main(String[] args){

        AccountImpl ehabAccount = new AccountImpl("Ehab", 123);
        AccountImpl aishaAccount = new AccountImpl("Aisha", 4444);
        BankSystem bankSystem = new BankSystem();
        bankSystem.register(ehabAccount);
        bankSystem.register(aishaAccount);
        bankSystem.showMenu();
        bankSystem.printAccounts();
        try {
            bankSystem.csvFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}